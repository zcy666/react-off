import React, { useEffect, useState } from "react";
import "./pdf.model.css";
// import {Document,Page} from 'react-pdf/dist/esm/entry.webpack'
// import FileView from 'react-file-viewer'
// import pdf from '../../assets/pdf/1.pdf'
import { useLocation, useNavigate, useParams, useSearchParams } from "react-router-dom";
import { Button } from "antd";
import {ReactComponent as BackIcon} from './back.svg'
function Pdf() {

  // const [search,setsearch] = useSearchParams()

  const names=useLocation()
  const navtor=useNavigate()
  
// useEffect(()=>{
// // console.log(pdf)
// console.log(names.search.split('=')[1])
// },[])
// const [numPages, setNumPages] = useState(null);
// const [pageNumber, setPageNumber] = useState(1);
// // pdf加载成功
// const onDocumentLoadSuccess = ({ numPages }) => {
//   setNumPages(numPages);
// };

  return <div className="pdf_app">
   {/* react-pdf方法展示pdf文件 分页需要自己手写 */}
    {/* <Document 
    // options={{
    //   cMapUrl:`//cdn.jsdelivr.net/npm/pdfjs-dist@${pdfjs.version}/cmaps/`,
    //   cMapPacked:true
    // }}
    file={pdf}
    onLoadSuccess={onDocumentLoadSuccess}
    >
        <Page pageNumber={pageNumber} scale={1}/>
    </Document> */}
    
    {/* iframe方法  简单方便 分页不需要自己手写*/}
    <Button onClick={()=>{navtor(-1)}} style={{zIndex:"99",position:"absolute",right:"9%",top:"1%",background:"rgba(0,0,0,0)",color:"white",border:"red",display:"flex",alignItems:"center"}}><BackIcon  fontSize={12} width={15} height={15} /><span style={{color:"Red"}}>返回</span></Button>
    <iframe style={{position:"relative"}} src={names.search.split('=')[1]} frameborder="0" width={100+'%'} height={95+'%'}></iframe>
  </div>;
}
export default Pdf;
