import React, { useEffect, useRef } from "react";
import "./present.model.css";
import { Button, Carousel,Image} from "antd";

import { ReactComponent as PresIcon } from "./介绍.svg";
import { ReactComponent as LeftIcon } from "./左.svg";
import { ReactComponent as RightIcon } from "./右.svg";
import cpuimg from "../../assets/img/6e5238f63229b368caac045d3f0e320.png";
import { useNavigate } from "react-router-dom";

import linterautreJson from "../../assets/Json/literature";
const contentStyle = {
  width: "100%",
  height: "23.75rem",
  color: "#red",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  marginBottom:"0.625rem"
};
function PresentIndex() {
  const navto = useNavigate();
 
  const employeeRef = useRef();
  
  return (
    <div className="present_div">
      <div className="present_text_div">
        <div className="present_text">
          <h1>
            <PresIcon />
          </h1>
          <div className="BigTtile" style={{ fontSize: "1.25rem" }}>
            <p>
              北京百奥华兴基因科技有限公司成立于2023年8月，位于北京市大兴区，公司致力于基因测序和多组学大数据计算软件研发，服务于生命科学基础设施平台建设，近年来不断深耕科技服务、临床医学和生物育种行业。团队成员来自顶尖高校，先后在NG、NC、PNAS和MP等学术期刊发表相关文章上百篇。
            </p>
            <img src={require("../../assets/img/firm1.png")} alt="" />
          </div>
        </div>
      </div>
      <div className="high_performance">
        <div className="high_child">
          <h1>高性能超算中心</h1>
          <div className="high_text">
            <p style={{ fontSize: "1.25rem", lineHeight: "2.5rem" }}>
              百奥华兴高性能超算中心（High Performance Supercomputer Center,
              HPSC）采用A100 GPU、Intel
              9代CPU和分布式存储的高效组合，实现极速稳定的基因大数据分析、算法实现及交付。本地和云计算资源高效支撑智能育种运算平台。随着公司业务的发展，高性能超算中心将会持续升级，
              以保证高效的基因大数据处理和安全的数据存储。 13，040个物理核数
              2,526 Tflops 计算峰值速度 80 TB 总内存 10PB 总储存
            </p>
            <img src={cpuimg} width={100 + "%"} height={80 + "%"} alt="" />
          </div>
        </div>
      </div>
      <div className="text_div">
        <h1>文章一览</h1>
        <div className="text_child">
          {linterautreJson.map((v, i) => {
            return (
              <div
                className="literature_item"
                key={v.id}
                onClick={() => {
                  navto(v.path + v.src);
                }}
              >
                <img
                  src={require("../../assets/img/" + v.img + ".png")}
                  alt=""
                />
                <p>发表期刊:</p>
                <p>发表时间:</p>
              </div>
            );
          })}
        </div>
      </div>
      <div className="Comfortable_office">
        <div className="Comfortable_office_child">
          <h1 className="offic_title">舒适办公</h1>
          <div className="offic_imgItem">
            <div className="img_top">
              <Image
                src={require("../../assets/img/firm1.png")}
                width={50 + "%"}
                style={{
                  marginRight:"0.625rem"
                }}
                alt=""
              />
              <Image
                src={require("../../assets/img/firm3.png")}
                width={15 + "%"}
                style={{
                  marginLeft:"0.625rem"
                }}
                alt=""
              />
            </div>
            <div className="img_center">
              <Image
                src={require("../../assets/img/firm2.png")}
                width={40 + "%"}
                style={{
                  marginRight:"0.625rem"
                }}
                alt=""
              />
              <Image
                src={require("../../assets/img/firm4.png")}
                width={40 + "%"}
                style={{
                  marginLeft:"0.625rem"
                }}
                alt=""
              />
            </div>
            <div className="img_bottom">
              <Image
                src={require("../../assets/img/firm7.png")}
                width={17 + "%"}
                
                alt=""
              />
              <Image
                src={require("../../assets/img/firm8.png")}
                width={30 + "%"}
                style={{
                  margin:"0 0.625rem"
                }}
                alt=""
              />
              <Image
                src={require("../../assets/img/firm9.png")}
                width={30 + "%"}
                  style={{
                    marginLeft:"0.9375rem"
                  }}
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
      <div className="off_tema">
        <div className="off_tema_child">
          <h1>公司团队</h1>
          <div className="boss_present">
            <div className="left">
              <p className="boss_job_position">创始人</p>
              <p className="boss_name">
                <span>梁齐齐</span>
                <span>副研究员</span>
                <span>董事长 ＆ CEO</span>
              </p>
              <p className="boss_content">
              毕业于华中农业大学，北京市生物医药研究专业副研究员，十多年的基因组学大数据研究经验，专注于高通量基因测序大数据的软件和产品开发，以一作或通讯作者在PNAS, MP和GPB等期刊发表10余篇研究论文，申请软著和专利多项。
              </p>
            </div>
            <div className="right">
              <img src={require('../../assets/img/lqq.png')}
              width={35+'%'}
              height={35+'%'}
              alt="" />
            </div>
          </div>
          <div className="employee_div">
            <p className="employee_title">技术团队</p>
            <Carousel
            dots={false}
              ref={employeeRef}
              style={{
                position: "absolute",
                top: "0",
                left: "0",
                width: "100%",
                height: "23.75rem",
                top: "40%",
              }}
           
            >
              <div>
                <div style={contentStyle}>
                  <div className="employee_item">
                    <img src={require('../../assets/img/wcy.png')}
                    style={{
                      width:"25%",
                      height:"40%"
                    }}
                    alt="" />
                    <p className="employee_name">王翠颖</p>
                    <p className="employee_position">产品总监</p>
                    <p className="employee_content">华中农业大学国家重点实验室硕士生，近十年来专注于出生缺陷防控和肿瘤个体化诊治领域的技术研发和转化研究，拥有丰富的耳聋基因筛查、遗传病诊断和液体活检ctDNA动态监测等临床项目经验，具有项目管理专业人士资格和遗传咨询师认证。</p>
                  </div>
                  <div className="employee_item">
                    <img src={require('../../assets/img/wm.png')}
                    style={{
                      width:"25%",
                      height:"40%"
                    }}
                    alt="" />
                    <p className="employee_name">王猛</p>
                    <p className="employee_position">研发总监</p>
                    <p className="employee_content">云南大学国家重点实验室硕士研究生，主攻家养动物进化和临床医学方向，其中多中心癌症早筛项目经验丰富。拥有十年的大数据生物信息学领域经验，并发表过多篇文章和专利。</p>
                  </div>
                  <div className="employee_item">
                    <img src={require('../../assets/img/yy.png')}
                    style={{
                      width:"34%",
                      height:"40%"
                    }}
                    alt="" />
                    <p className="employee_name">喻宇烨</p>
                    <p className="employee_position">技术总监</p>
                    <p className="employee_content">华中农业大学作物遗传育种专业硕士，近十年专注于基因组学大数据方向，在分子育种和基因组选择育种等生物育种方向经验丰富，发表过一系列相关软著、专利和文章累计达数十篇。</p>
                  </div>
                </div>
              </div>
              <div>
                <div style={contentStyle}>
                <div className="employee_item">
                    <img src={require('../../assets/img/yhz.png')}
                    style={{
                      width:"25%",
                      height:"40%"
                    }}
                    alt="" />
                    <p className="employee_name">于海至</p>
                    <p className="employee_position">运营总监</p>
                    <p className="employee_content">华中农业大学国家重点实验室硕士，拥有十多年基因和质谱领域项目经验，其中在项目管理上经验丰富，并搭建一整套成熟的、广受客户认可的基因科技上市企业的运营交付体系。</p>
                  </div>
                  <div className="employee_item">
                    <img src={require('../../assets/img/zcy.png')}
                    style={{
                      width:"25%",
                      height:"40%"
                    }}
                    alt="" />
                    <p className="employee_name">张春阳</p>
                    <p className="employee_position">信息化总监</p>
                    <p className="employee_content">毕业于华北理工大学，多年的智慧平台开发经验，精通多种开发语言，专注于校园、医疗机构和药企的智慧管理平台开发。</p>
                  </div>
                  <div className="employee_item" style={{background:"#fff"}}></div>
                </div>
              </div>
              {/* <div>
                <div style={contentStyle}>
                <div className="employee_item"></div>
                  <div className="employee_item"></div>
                  <div className="employee_item"></div>
                </div>
              </div> */}
              {/* <div>
                <div style={contentStyle}>
                <div className="employee_item"></div>
                  <div className="employee_item"></div>
                  <div className="employee_item"></div>
                </div>
              </div> */}
            </Carousel>
            <Button
              onClick={() => {
                employeeRef.current.prev();
              }}
              className="employee_leftButton"
            >
              <LeftIcon style={{background: "transparent"}} />
            </Button>
            <Button
              onClick={() => {
                employeeRef.current.next();
              }}
              className="employee_rightButton"
            >
              <RightIcon />
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PresentIndex;
