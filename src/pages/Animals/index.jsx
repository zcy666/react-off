import React from "react";
import "./Animals.model.css";
function AnimalsIndex() {
  return (
    <div className="Animals_app">
      <div className="animals_title">
        <h1>动植物全基因组重测序</h1>
      </div>
      <div className="Product_overview">
        <div className="product_child">
          <p>产品概述</p>
          <p>
            动植物全基因组重测序是对已知基因组序列的物种进行DNA测序，并在此基础上完成个体或群体分析。通过序列比对，可以检测到大量变异信息，包括单核苷酸多态性位点（SNP），插入缺失位点（Insertion/Deletion,
            InDel）、结构变异（Structure Variation, SV）位点，拷贝数变异（Copy
            Number Variation,
            CNV）位点等，获得同一物种不同个体的遗传变异图谱。利用全基因组重测序技术有助于快速发现与动植物重要性状相关的遗传变异，应用于分子育种中，缩短育种周期。
          </p>
        </div>
      </div>
      <div className="product_good">
        <div className="good_child">
          <p>产品优势</p>
          <div className="good_item">
            <img src={require("../../assets/img/图片66.png")} alt="" />
            <p>
              <span>经验丰富、数据准确</span>
              <span>至今完成多个物种的重测序样本，拥有丰富的分析经验</span>
            </p>
          </div>
          <div className="good_item">
            <img src={require("../../assets/img/图片67.png")} alt="" />
            <p>
              <span>自主测序平台、成本可控</span>
              <span>
                滚环扩增构建DNB测序文库，PCR-free重测序检测InDel更准确，无index
                hopping之忧，低dup rate无需人为干预{" "}
              </span>
            </p>
          </div>
        </div>
      </div>
      <div className="product_app">
        <div className="product_app_child">
          <p>产品应用</p>
          <div className="product_app_item">
            <img src={require("../../assets/img/图片68.png")} alt="" />
            <img src={require("../../assets/img/图片69.png")} alt="" />
            <img src={require("../../assets/img/图片70.png")} alt="" />
            <img src={require("../../assets/img/图片71.png")} alt="" />
            <img src={require("../../assets/img/图片72.png")} alt="" />
          </div>
        </div>
      </div>
      <div className="research_contents">
        <div className="research_contents_child">
          <p>研究内容</p>
          <img
            src={require("../../assets/img/图片74.png")}
            height={100 + "%"}
            alt=""
          />
        </div>
      </div>
    </div>
  );
}

export default AnimalsIndex;
