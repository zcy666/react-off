import React, { useEffect, useRef, useState } from "react";
import "./Home.model.css";
import { Carousel, Button } from "antd";
import { ReactComponent as RightIcon } from "./向右1.svg";
import { useNavigate } from "react-router-dom";
import {ReactComponent as LeftIcon} from './左.svg'
import {ReactComponent as RightSwierIcon} from './右.svg'
import newList from '../../assets/Json/newscenter'
const contentStyle = {
  width: "100%",
  height: "37.5rem",
  color: "#fff",
  textAlign: "center",
  background: "#364d79",
  zIndex: "-1",
  display: "flex",
  alignItems: "center",
  justifyContent: 'space-around',
  position: "relative",
};

function HomeIndex() {
  const navtor = useNavigate();
  // const onChange = (currentSlide) => {
  //   console.log(currentSlide);
  // };
  const swiperRef = useRef();
  useEffect(()=>{
    console.log(newList)
  },[])
  return (
    <div className="Home_app">
      <Carousel ref={swiperRef}>
        <div>
          <Button
          className="Home_left_button"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              position: "absolute",
              top: "46.6%",
              left:"21%",
              width: "2.5rem",
              height: "2.5rem",
              borderRadius: "50%",
              border:"none",
              color:"white",
            }}
            onClick={()=>{
              swiperRef.current.prev()
            }}
          >
            《
          </Button>
          <div style={contentStyle}>
            <img
              src={require("../../assets/img/图片23.png")}
              width={100 + "%"}
              height={100 + "%"}
              alt=""
            />
            <div
              style={{
                zIndex: "99",
                display: "flex",
                alignItems: "center",
                position: "absolute",
                width: "45%",
                height: "30%",
                background: "rgba(0,0,0,0.5)",
                left: "5%",
              }}
            >
              <div className="Home_left">
                
              </div>
              <div className="Home_center">
              <p>
              基于GPU、CPU和分布式存储的高性能超算中心
              </p>
              <p>（High Performance Supercomputer Center, HPSC）</p>
              </div>
              <div className="Home_right"></div>
            </div>
          </div>
          <Button
          className="Home_right_button"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              position: "absolute",
              top: "46.6%",
              left: "29.6%",
              width: "2.5rem",
              height: "2.5rem",
              borderRadius: "50%",
              border:"none",
              color:"white"
            }}
            onClick={()=>{
              swiperRef.current.next()
            }}
          >
            》
          </Button>
        </div>
        <div>
        <Button
        className="Home_left_button"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              position: "absolute",
              top: "46.6%",
              left:"41.1%",
              width: "2.5rem",
              height: "2.5rem",
              borderRadius: "50%",
              color:"white",
              border:"none"
            }}
            onClick={()=>{
              swiperRef.current.prev()
            }}
          >
            《
          </Button>
          <div style={contentStyle}>
            <img
              src={require("../../assets/img/图片26.png")}
              width={30 + "%"}
              height={100 + "%"}
              alt=""
            />
            <img
              src={require("../../assets/img/图片25.png")}
              width={30 + "%"}
              height={100 + "%"}
              alt=""
            />
            <img
              src={require("../../assets/img/图片24.png")}
              width={30 + "%"}
              height={100 + "%"}
              alt=""
            />
                <div
              style={{
                zIndex: "99",
                display: "flex",
                alignItems: "center",
                position: "absolute",
                width: "40%",
                height: "30%",
                background: "rgba(0,0,0,0.5)",
                left: "5%",
              }}
            >
              <div className="Home_left"></div>
              <div className="Home_center">
              <p>DNBSEQ-T7、PacBio Revio旗舰</p>
              <p>和Illumina X系列测序平台</p>
              </div>
              
              <div className="Home_right"></div>
            </div>
          </div>
          <Button
          className="Home_right_button"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              position: "absolute",
              top: "46.6%",
              left: "48.54%",
              width: "2.5rem",
              height: "2.5rem",
              borderRadius: "50%",
              border:"none",
              color:"white"
            }}
            onClick={()=>{
              swiperRef.current.next()
            }}
          >
            》
          </Button>
        </div>
      </Carousel>

    
      <div className="about_div">
        <div className="about_child">
          <div className="context">
            <p style={{ fontSize: "1.25rem", color: "black" }}>百奥华兴</p>
            <p style={{ fontSize: "1.875rem", color: "black", margin: "1.875rem 0" }}>
              为您提供领先的基因大数据解决方案
            </p>
            <p
              style={{ color: "black", marginBottom: "1.875rem", fontSize: "0.75rem" }}
            >
              北京百奥华兴基因科技有限公司成立于2023年8月，位于北京市大兴区，公司致力于基因测序和多组学大数据计算软件研发，服务于生命科学基础设施平台建设，近年来不断深耕科技服务、临床医学和生物育种行业。团队成员来自顶尖高校，先后在NG、NC、PNAS和MP等学术期刊发表相关文章上百篇。
            </p>
            <a
              href="/present"
              style={{
                margin: "1.875rem 0",
                width: "11.25rem",
                height: "2.1875rem",
                border: "0.0625rem solid #595757",
                fontSize: "0.75rem",
                borderRadius: "5px",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                color: "#595757",
              }}
            >
              了解更多
            </a>
          </div>
          <div className="img_div">
            <img
              src={require("../../assets/img/firm6.png")}
              width={50 + "%"}
              height={80 + "%"}
              alt=""
            />
          </div>
        </div>
      </div>
      <div className="skill_div">
        <div className="skill_child">
          <div className="skill_left">
            <img
              src={require('../../assets/img/图片77.png')}
              width={70 + "%"}
              height={80 + "%"}
              style={{
                borderRadius:"0.625rem"
              }}
              alt=""
            />
          </div>
          <div className="skill_right">
            <p style={{ fontSize: "1.25rem", color: "#595757" }}>前沿技术</p>
            <p
              style={{
                fontSize: "1.875rem",
                color: "#003E7A",
                margin: "1.875rem 0",
                display: "flex",
                flexDirection: "column",
              }}
            >
              <span>智能选育平台</span>
              <span>————Falcon</span>
            </p>
            <p style={{ fontSize: "0.75rem", color: "#ccccc", margin: "1.875rem 0" }}>
              进一步定义行业内 NGS
              测序标准，为客户提供更智能、更高效、更可靠的服务，实现生产效率、稳定性和质量的全面突破。
            </p>
            
              <a style={{
                margin: "1.875rem 0",
                width: "11.25rem",
                height: "2.1875rem",
                border: "0.0625rem solid #595757",
                fontSize: "0.75rem",
                borderRadius: "5px",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                color: "#595757",
              }} href="/Intellgent">了解更多></a>
           
          </div>
        </div>
      </div>
      <div className="news_div">
        <div className="news_child">
          <p
            style={{
              width: 100 + "%",
              height: "10%",
              display: "flex",
              alignItems: "center",
              color: "#595757",
              fontSize: "1.25rem",
              paddingLeft: "0.625rem",
            }}
          >
            新闻中心
          </p>
          <div className="news_dl">
         {
          newList.map((v,i)=>{
            return (
              <dl key={v.id}>
              <dt>
                <img src={require(`../../assets/img/图片${v.img}.png`)} alt="" />
              </dt>
              <dd>
                <p>{v.time}</p>
                <p>
                {v.title}
                </p>
                <p ><a href={'/newcontent?name='+v.id} >了解更多></a></p>
              </dd>
            </dl>
            )
          })
         }
          </div>
        </div>
      </div>
    </div>
  );
}

export default HomeIndex;