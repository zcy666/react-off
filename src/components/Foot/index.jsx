import React from "react";
import "./footer.model.css";
import LogoImg from "../../assets/logo2.png";
import TwoCode from "../../assets/图片1.png";
// import LogoTwo from "../../assets/logo.png"
function FooterIndex() {
  return (
    <div className="footer_div">
      <div className="footer_top">
        <div className="footer_top_child">
          <div className="footer_top_left">
            <img
              src={require("../../assets/logo2.png")}
              // height={20 + "%"}
              width={60 + "%"}
              alt=""
            />
            <p
              style={{
                width: "100%",
                height: "10%",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              电话:18322280330
            </p>
            <p
              style={{
                width: "100%",
                height: "10%",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              邮箱:support@biohuaxing.com
            </p>
            <div className="two_code">
              <p>公众号</p>
              <img src={require("../../assets/图片1.png")} alt="" />
            </div>
          </div>
          <div className="footer_top_right">
            <div className="right_item">
              <p className="title">关于百奥华兴</p>
              <p className="title_bro">
                <a target="" rel="noopener noreferrer" href="/present">
                  简介
                </a>
              </p>
              
              <p className="title_bro">
                <a target="" rel="nooper noreferrer" href="/techno">
                  技术平台
                </a>
              </p>
              <p className="title_bro">
                <a target="" rel="nooper noreferrer" href="/research">
                  研究成果
                </a>
              </p>
              <p className="title_bro">
                <a target="" rel="nooper noreferrer" href="/investore">
                  投资者关系
                </a>
              </p>
              <p className="title_bro">
                <a target="" rel="nooper noreferrer" href="/patent">
                  资质专利
                </a>
              </p>
              <p className="title_bro">
                <a target="" rel="nooper noreferrer" href="/newscenter">
                  新闻中心
                </a>
              </p>
              <p className="title_bro">
                <a target="" rel="nooper noreferrer" href="/contact">
                  联系我们
                </a>
              </p>
            </div>
            <div className="right_item">
              <p className="title">科技服务产品</p>
              <p className="title_bro">
                <a target="" rel="noopener noreferrer" href="/Illm">
                  Illumina/T7 二代测序
                </a>
              </p>
              <p className="title_bro">
                <a target="" rel="noopener noreferrer" href="/pacbio">
                  PacBio/ONT 三代测序
                </a>
              </p>
              <p className="title_bro">
                <a target="" rel="noopener noreferrer" href="/Genomics">
                  基因组学分析及软件开发
                </a>
              </p>
              <p className="title_bro">
                <a target="" href="/Pang" rel="noopener noreferrer">
                  泛基因组测序（Pan-genome）
                </a>
              </p>
              <p className="title_bro">
                <a target="" rel="noopener noreferrer" href="/sequencing">
                  单细胞测序分析及软件开发
                </a>
              </p>
              <p className="title_bro">
                <a target="" rel="noopener noreferrer" href="/analysis">
                  多组学大数据联合分析
                </a>
              </p>
              <p className="title_pro">
                <a target="" rel="noopener noreferrer" href="/Functional">
                  质谱分析
                </a>
              </p>
              <p className="title_bro">
                <a target="" rel="noopener noreferrer" href="/Resources">
                  种质资源评价
                </a>
              </p>
              <p className="title_bro">
                <a target="" rel="noopener noreferrer" href="/Mole">
                  分子身份证开发
                </a>
              </p>
              <p className="title_bro">
                <a target="" rel="noopener noreferrer" href="/animals">
                  动植物全基因组重测序
                </a>
              </p>
            </div>
            <div className="right_item">
              <p className="title">生物育种大数据平台开发</p>
              <p className="title_bro">
                <a target="" rel="noopener noreferrer" href="/Excavate">
                  物种多组学大数据功能位点挖掘平台开发
                </a>
              </p>
              <p className="title_bro">
                <a target="" rel="noopener noreferrer" href="/Intellgent">
                  基因组选择及最优亲本选配智能平台开发
                </a>
              </p>
            </div>
            <div className="right_item">
              <p className="title">药物研发</p>
              <p className="title_bro">
                <a target="" rel="noopener noreferrer" href="/Deveopt">
                  基因编辑工具开发及优化
                </a>
              </p>
              <p className="title_bro">
                <a target="" rel="noopener noreferrer" href="/Detection">
                  脱靶检测
                </a>
              </p>
              <p className="title_bro">
                <a target="" rel="noopener noreferrer" href="/Transformation">
                  工程菌改造
                </a>
              </p>
              <p className="title_bro">
                <a target="" rel="noopener noreferrer" href="/Protein">
                  蛋白结构解析
                </a>
              </p>
            </div>
            <div className="right_item">
              <p className="title">生命科学基础设施平台</p>
              <p className="title_bro">
                <a target="" rel="noopener noreferrer" href="/Variation">
                  高通量测序变异检测软件算法和硬件加速（BWA、GATK等）
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="footer_bottom">
        Copyright@2011-2023 All Rights Reserved
        版权所有：北京百奥华兴基因科技有限公司 京ICP备15007085号-1
      </div>
    </div>
  );
}

export default FooterIndex;
