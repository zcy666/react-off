import React, { useEffect, useState } from 'react'
import "./newtext.model.css"
import newList from  "../../assets/Json/newscenter"
import TextIndex from '../TextIndex/text'
function NewsText(props) {
    const {id}=props
    const [newtext,setNewText]=useState({});
    
    useEffect(()=>{
        setNewText(newList[id])
    },[])   
    
    // 判断id 根据id判断该展示什么新闻
    const IsText=()=>{
        if(id==='0'){
            return <TextIndex/>
        }
    }
  return (
    <div className='news_text'>
        <div className='news_text_title'>
            <h2>{newtext.title}</h2>
        </div>
        <div className='time_div'>
           <div className="time_div_child">
           <p>{newList[id].time}</p>
           <p>编辑：百奥华兴</p>
           </div>
        </div>
        {
            IsText()
        }  
    </div>
  )
}

export default NewsText