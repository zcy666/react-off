import React, { useEffect, useState } from 'react'
// "proxy": "http://47.93.101.203/",
import './head.model.css'
// import Tech from '../../assets/Json/itemOne'
import { useNavigate } from 'react-router-dom'
import LogoImg from '../../assets/logo.jpg'


import TechIndex from '../Tech';
import TecgData from '../../assets/Json/itemOne'

import Breeding from '../../assets/Json/itemTwo'

import  drug from '../../assets/Json/itemThree'

import life from '../../assets/Json/itemFour'

import about from '../../assets/Json/itemFive'
function Header() {
  const navtor = useNavigate()

  return (
    <div className='head_div'>
      <div className='head_left' onClick={()=>{navtor('/')}}><a href='/'><img src={LogoImg} width={30+'%'} height={"90%"} alt="" /></a></div>
      <ul className='head_list'>
        <li><a style={{textDecoration:"none",color:"#000",color:"#000"}} href="/">首页</a></li>
        <li>
         <TechIndex items={TecgData} title='科技服务产品' />
        </li>
        <li> <TechIndex items={Breeding} title='生物育种大数据平台开发' /></li>
        <li><TechIndex items={drug} title='药物研发' /></li>
        <li><TechIndex items={life} title='生命科学基础设施平台' /></li>
        <li><TechIndex items={about} title='关于百奥华兴'/></li>
        <li><a target='' href='/contact' style={{fontSize:"0.875rem"}}>联系我们</a></li>
      </ul>
    </div>
  )
}

export default Header
