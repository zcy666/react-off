import React, { useEffect } from 'react'
import { DownOutlined } from '@ant-design/icons';
import { Dropdown, Space } from 'antd';

function TechIndex(props) {
  const {items,title}=props

  return (
    <div>
       <Dropdown
            menu={{
              items,
            }}
          >
            <a onClick={(e) => e.preventDefault()}>
              <Space>
              <span style={{fontSize:"0.875rem"}}>{title}</span>
                <DownOutlined />
              </Space>
            </a>
          </Dropdown>
    </div>
  )
}

export default TechIndex
