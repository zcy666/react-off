const itemThree=[
  {
    "key": "1",
    "label": (
      <a target="" rel="noopener noreferrer" href="/Deveopt">
        基因编辑工具开发及优化
      </a>
    )
  },
  {
    "key": "2",
    "label": (
      <a target="" rel="noopener noreferrer" href="/Detection">
        脱靶检测
      </a>
    )
  },
  {
    "key": "3",
    "label": (
      <a target="" rel="noopener noreferrer" href="/Transformation">
        工程菌改造
      </a>
    )
  },
  {
    "key": "4",
    "label": (
      <a target="" rel="noopener noreferrer" href="/Protein">
        蛋白结构解析
      </a>
    )
  }
]

export default itemThree