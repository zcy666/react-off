const textJson=[
    {
        "id":"0",
        'one':"12月15日，2023“数启京津·智汇武清”清数科技园推介会暨重点项目签约仪式在北京隆重举行，吸引了百奥华兴以及众多企业共同参与。与会者们共同探讨大数据行业的未来趋势，汇聚产业链上下游的合作伙伴，旨在共同推进京津冀协同发展背景下数字经济产业的繁荣。",
        'two':"本次活动在天津市人民政府驻北京办事处、武清区推进京津冀协同发展领导小组办公室的指导下，由武清区工信局、高村科技创新园主办，瀚海武清国际创新中心、武清大数据产业创新联盟承办。天津市驻京办、武清区协同办、人才办、工信局、科技局、网信办等相关负责人参加。活动旨在隆重推介清数科技园，举行重点项目签约仪式。 ",
        'three':"天津将“推进京津冀协同发展走深走实”作为市委市政府“十项行动”之首，武清区作为京津走廊上的重要节点，更是京津冀协同发展的桥头堡和主力军。天津市驻京办协同发展联络处处长尚国维在致辞中说：“高村科技园作为数字经济发展的重要平台，迎来了前所未有的机遇”未来，天津将继续大力优化京津冀区域产业布局，持续推进三地协同，相信武清的数字经济定会收获喜人的发展成果。",
        
    }
]